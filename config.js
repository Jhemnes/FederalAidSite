'use strict';
const Confidence = require('confidence');

const criteria = {
    env: process.env.NODE_ENV
};


const config = {
    $meta: 'This file configures the plot device.',
    projectName: 'FederalEligibilitySite',
    port: {
        web: {
            $filter: 'env',
            test: 9090,
            production: 8081,
            $default: 8081
        }
    }
};


const store = new Confidence.Store(config);


exports.get = function (key) {

    return store.get(key, criteria);
};


exports.meta = function (key) {

    return store.meta(key, criteria);
};
